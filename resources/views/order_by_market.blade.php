<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<form method='get' id='form-market' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['mid']) !!}
    <div class="input-group">
        <select onchange="$('#form-market').submit()" name='mid' style="width: 300px;" class='form-control input-sm'>
        <option {{($status=="all")?'selected':''}} value='all'>- All -</option>
        @foreach($market as $row)
        {{$mid=$request_mid}}
         <option {{($mid==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' id='form-status' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['status']) !!}
    <div class="input-group">
        <select onchange="$('#form-status').submit()" name='status' style="width: 300px;" class='form-control input-sm'>
        <option {{($status=="all")?'selected':''}} value='all'>- All -</option>
        @foreach($orders_status as $row)
        {{$status=$request_status}}
         <option {{($status==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name_in_agent}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
    <div class="input-group">
        <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}"
                placeholder="{{trans('crudbooster.filter_search')}}"/>
        {!! CRUDBooster::getUrlParameters(['q']) !!}
        <div class="input-group-btn">
            @if(Request::get('q'))
                <?php
                $parameters = Request::all();
                unset($parameters['q']);
                $build_query = urldecode(http_build_query($parameters));
                $build_query = ($build_query) ? "?".$build_query : "";
                $build_query = (Request::all()) ? $build_query : "";
                ?>
                <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"'
                        title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
            @endif
            <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<table class='table table-striped table-dark'>
  <thead>
      <tr>
        <th>No.</th>
        <th>Pasar</th>
        <th>Ref Number</th>
        <th>Customer</th>
        <th>Squad</th>
        <th>Status</th>
        <th>Total Amount</th>
        <th>Fee Admin</th>
        <th>Fee Delivery</th>
        <th>Fee Captain</th>
        <th>Fee Squad</th>

        <th>Total Payment</th>
        <th>Total Payment di Agent</th>
        <th>Total Reversal</th>
        <th>Payment Method</th>
        <th>Payment Status</th>
        <th>Last Update</th>
        <th>Action</th>
       </tr>
  </thead>
  <tbody>
    <?php $i=1;
    ?>
    
    @foreach($result as $row)
      <?php
      $link=CRUDBooster::adminPath()."/orders_detail?q=".$row->reference;
      ?>
      <tr>
        <td>{{$i++}}.</td>
        <td>{{$row->pasar}}</td>
        <td><a href="{{$link}}">{{$row->reference}}</a></td>
        <td>{{$row->customer}}</td>
        <td>{{$row->squad}}</td>
        <td>{{$row->status}}</td>  
        <td>{{$row->total_amount}}</td>
        <td>{{$row->fee_product}}</td>
        <td>{{$row->fee_delivery}}</td>
        <td>{{$row->fee_aggregator}}</td>
        <td>{{$row->fee_agent}}</td> 
        <td>{{$row->payment_total}}</td>
        <td>{{$row->payment_total_origin}}</td>
        <td>{{$row->payment_total_reversal}}</td>
        <td>{{$row->payment_method}}</td>
        <td>{{$row->payment_status}}</td>
        <td>{{$row->updated_at}}</td>
        <td>
          <!-- To make sure we have read access, wee need to validate the privilege -->
          @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif
        </td>
       </tr>
     
    @endforeach
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection