<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Ngepasar.com - Simulator</title>

    <!--<link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/starter-template/">-->

    <!-- Bootstrap core CSS -->
    <!--<link href="../assets/dist/css/bootstrap.css" rel="stylesheet">-->
    <link href="{{ asset("vendor/bootstrap/assets/dist/css/bootstrap.css") }}" rel="stylesheet" type="text/css"/>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/home.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>

    $(document).ready(function(){
     
    
      function readURL(input,preview) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#'+preview).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
          }
        }

      $("#nik_photo").change(function() {
        readURL(this,'p_photo_ktp');
      });
      $("#npwp_photo").change(function() {
        readURL(this,'p_photo_npwp');
      });
      $("#account_photo").change(function() {
        readURL(this,'p_account_photo');
      });

    });

    
    
      </script>

  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-success fixed-top">
  <a class="navbar-brand" href="https://ngepasar.com"><img src="img/logo.png"/>&nbsp;Ngepasar.com</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
       <li class="nav-item active">
        <a class="nav-link" href="register">Registrasi<span class="sr-only">(current)</span></a>
       </li>
       <li class="nav-item active">
        <a class="nav-link" href="simulator">Simulator<span class="sr-only"></span></a>
       </li> 
    </ul>
    <form class="form-inline my-2 my-lg-0" method="get" action="{{CRUDBooster::adminPath()}}">
      <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
      <button class="btn btn-light my-2 my-sm-0" type="submit">login</button>
    </form>
  </div>
</nav>

<main role="main" class="container">
  <div class="row">
    <h1>Registrasi Sebagai Captain</h1>
  </div>
  <div class="row">
    <p>Setelah melakukan registrasi, akun akan diproses dan informasi akan dikirim melalui whatsapp atau email.<br>Informasi lebih lanjut hubungi <a href="https://api.whatsapp.com/send?phone=6282115770574&text=Tanya+registrasi+captain+ngepasar">08211xxxxxx</a></p>
  </div>
<hr>
  <!--<div class="row">
    <h2>Silakan isi informasi di bawah ini :</h2>
  </div>
  -->
  @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
    @endif

    @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
      </div>
    @endif

  <form action="{{url('registerSubmit')}}" method="POST" enctype="multipart/form-data">
  @csrf
  {{ csrf_field() }}
  <div class="row">
    <div class="col-sm-4 form-group">
          <div class="row">
              <label for="market">Name :</label>
              <input type="input" class="form-control" name="name" required>
          </div>
          <div class="row">
              <label for="trx">Email:</label>
              <input type="input" class="form-control" name="email" required>
          </div>
          <div class="row">
              <label for="exp">Phone:</label>
              <input type="input" class="form-control" name="phone" required>
          </div>
         
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-4 form-group">
        <div class="row">
              <label for="exp">Address:</label>
              <input type="input" class="form-control" name="address" required>
          </div>
          <div class="row">
              <label for="exp">City:</label>
              <select class="form-control" name="city" id="city" required>
              @foreach($city as $row)
                 <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
              </select>
          </div>
          <div class="row">
              <label for="exp">Province:</label>
              <select class="form-control" name="province" id="province" required>
              @foreach($province as $row)
                 <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
              </select>
          </div>
         
    </div>
  </div>

  <hr>
  
  <div class="row">
 <!--   <h2>Data Pelengkap</h2>-->
  </div>
  <div class="row">
    <div class="col-sm-4 form-group">
           <div class="row">
              <label for="daily_fee_squad">NIK KTP:</label>
              <input type="text" class="form-control" name="nik" required>
          </div>
          <div class="row">
              <label for="daily_fee_squad">Photo KTP:</label>
              <input type="file" class="form-control" id="nik_photo" name="nik_photo" required>
              <img id="p_photo_ktp" class="img-fluid img-thumbnail" width="400px" height="200px" />
          </div>
          <div class="row">
              <label for="daily_fee_squad">Nomor NPWP:</label>
              <input type="text" class="form-control" name="npwp">
          </div>
          <div class="row">
              <label for="daily_fee_squad">Photo NPWP:</label>
              <input type="file" class="form-control" id="npwp_photo" name="npwp_photo" required>
              <img id="p_photo_npwp" class="img-fluid img-thumbnail" width="400px" height="200px" />
          </div>
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-4 form-group">
           <div class="row">
              <label for="daily_fee_squad">Metode Pencairan:</label>
              <select class="form-control" name="settlement_id" id="settlement_id" required>
              @foreach($settlement as $row)
                 <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
              </select>
          </div>
          <div class="row">
              <label for="daily_fee_squad">No. Rekening:</label>
              <input type="text" class="form-control" name="account">
          </div>
          <div class="row">
              <label for="daily_fee_squad">Photo Buku Tabungan *khusus via bank:</label>
              <input type="file" class="form-control" id="account_photo" name="account_photo" required>
              <br>
              <img id="p_account_photo" class="img-fluid img-thumbnail" width="400px" height="200px" />
          </div>
          <div class="row">
              <label for="daily_fee_squad">PKS (Perjanjian Kerja Sama) File Upload:</label>
              <input type="file" class="form-control" id="pks_file" name="pks_file" required>
              <br>            
          </div>
    </div>
 </div>
  <div class="row">
    <button class="btn btn-success btn-block" type="submit">Submit</button>  
  </div>
  <div class="row">
   &nbsp;
  </div>
  
</form>



</main><!-- /.container -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="{{ asset("vendor/bootstrap/assets/dist/js/bootstrap.bundle.js") }}"></script></body>
</html>
