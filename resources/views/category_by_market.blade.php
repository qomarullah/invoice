<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<form method='get' id='form-market' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['mid']) !!}
    <div class="input-group">
        <select onchange="$('#form-market').submit()" name='mid' style="width: 300px;" class='form-control input-sm'>
        @foreach($market as $row)
        {{$mid=$request_mid}}
         <option {{($mid==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
    <div class="input-group">
        <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}"
                placeholder="{{trans('crudbooster.filter_search')}}"/>
        {!! CRUDBooster::getUrlParameters(['q']) !!}
        <div class="input-group-btn">
            @if(Request::get('q'))
                <?php
                $parameters = Request::all();
                unset($parameters['q']);
                $build_query = urldecode(http_build_query($parameters));
                $build_query = ($build_query) ? "?".$build_query : "";
                $build_query = (Request::all()) ? $build_query : "";
                ?>
                <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"'
                        title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
            @endif
            <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        </div>
        <!--<div class="input-group-btn">
        <?php
         // $link=CRUDBooster::mainpath()."/enableAll/".$mid;
          ?>
          &nbsp;<button type='button' onclick='location.href="{{$link}}"' class='btn btn-sm btn-info'>Enable Semua Category!</i></button>
       </div>
       -->
    </div>
</form>

<table class='table table-striped table-dark'>
  <thead>
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Enable</th>
        <th>Active</th>
        <th>Sort</th>
        <th>Action</th>
       </tr>
  </thead>
  <tbody>
    <?php $i=1;
    ?>
    
    @foreach($result as $row)
        <?php
        $enable="No - <a href='market_category/enable/$mid/$row->id/$request_search'>Enable?</a>";
        $is_active="No"; // - <a href='market_products_admin/active/$mid/$row->id/1/$request_search'>Activate?</a>";
        $sort=$row->sort;
        ?>

      @foreach($market_category as $rowx)
        @if($rowx->category_id==$row->id) 
        <?php
          $enable="Yes";// - <a href='market_products_admin/enable/$mid/$row->id/$rowx->price/$request_search'>Disable?</a>";
          if($rowx->is_active=="1")
            $is_active="Yes - <a href='market_category/active/$mid/$row->id/0/$request_search'>Deactivate?</a>";
          else
            $is_active="No - <a href='market_category/active/$mid/$row->id/1/$request_search'>Activate?</a>";
          
          $sort=$rowx->sort;
          ?>
        @endif
     @endforeach
      <tr>
        <td>{{$i++}}.</td>
        <td>{{$row->name}}</td>
        <td>{!!$enable!!}</td>
        <td>{!!$is_active!!}</td>
         <td>{{$sort}}</td>
        <td>
          <!-- To make sure we have read access, wee need to validate the privilege -->
          @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif
        </td>
       </tr>
     
    @endforeach
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection