<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<form method='get' id='form-market' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['mid']) !!}
    <div class="input-group">
        <select onchange="$('#form-market').submit()" name='mid' style="width: 300px;" class='form-control input-sm'>
        @foreach($market as $row)
        {{$mid=$request_mid}}
         <option {{($mid==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
    <div class="input-group">
        <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}"
                placeholder="{{trans('crudbooster.filter_search')}}"/>
        {!! CRUDBooster::getUrlParameters(['q']) !!}
        <div class="input-group-btn">
            @if(Request::get('q'))
                <?php
                $parameters = Request::all();
                unset($parameters['q']);
                $build_query = urldecode(http_build_query($parameters));
                $build_query = ($build_query) ? "?".$build_query : "";
                $build_query = (Request::all()) ? $build_query : "";
                ?>
                <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"'
                        title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
            @endif
            <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<table class='table table-striped table-dark'>
  <thead>
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Username</th>
        <th>Is Leader</th>
        <th>Phone</th>
        <th>Status</th>
        <th>Action</th>
       </tr>
  </thead>
  <tbody>
    <?php $i=1;
    ?>
    
    @foreach($result as $row)
        <?php
          if($row->status=="1")$status="Active";
          if($row->status=="0")$status="Not Active";
          if($row->status=="2")$status="Suspend";

          if($row->is_leader=="1")$is_leader="Yes";
          else $is_leader="No";

        ?>

      <tr>
        <td>{{$i++}}.</td>
        <td>{{$row->fullname}}</td>
        <td>{{$row->username}}</td>
        <td>{{$is_leader}}</td>
        <td>{{$row->phone}}</td>
        <td>{{$status}}</td>
       
        <td>
          <!-- To make sure we have read access, wee need to validate the privilege -->
          @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif
        </td>
       </tr>
     
    @endforeach
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection