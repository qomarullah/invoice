
@foreach($invoice as $row)
<!DOCTYPE html>
<html>
<head>
<style>
.tablex {
    border-collapse: collapse;
}

.tdx {
    border: 1px solid black;
    padding: 3px;
}

</style>
</head>


<body>
<table style="width: 100%;">
	<tbody>
		<tr>
			<td style="width: 33.3333%;">
				<div style="text-align: center;">
					<strong>
						<span style="font-size: 24px;">Laporan Penerimaan ZISWAF</span>
					</strong>
					<div style="text-align: center;">
						<strong>
							<span style="font-size: 12px;">(Zakat/Infaq/Sadaqah)</span>
						</strong>
					</div>
				</div>
			</td>
			<td style="width: 18%;">
				<br>
				</td>
				<td style="width: 48.4179%; vertical-align: top; text-align: right;">
					<img src="https://saqu.or.id/wp-content/uploads/2019/03/logo-saqu.png" style="width: 300px;" class="fr-fic fr-dib fr-fir">
					</td>
				</tr>
				<tr>
					<td style="width: 33.3333%;">
						<div style="text-align: center;">
							<strong></strong>
						</div>
						<br>
						</td>
						<td style="width: 18%;">
							<br>
							</td>
							<td style="width: 48.4179%; text-align: right;">
								<div style="text-align: right;">
									<strong>Office</strong> Bumi Sawangan Indah 2, Blok D2 No. 90 RT 005 RW 010 Pengasinan, Sawangan Kota Depok Telp. 02518606240
								</div>
								<p>
									<strong>www.saqu.or.id</strong>
								</p>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="width: 99.8507%;">
								<br>
									<hr>
										<p>
											<br>
											</p>
										</td>
									</tr>
									<tr>
										<td style="width: 40.3333%;">Nomor  : <strong>{{$row->reference}}</strong>
											   <br>Tanggal : <strong>{{$row->date}}</strong>
												<br>Donatur : <strong>{{$row->name}}
                                    <br>
                                    </strong>Alamat   : <strong>{{$row->address}}</strong>&nbsp;
													</td>
													<td style="width: 18%;">
														<br>
														</td>
														<td style="width: 60.4179%;">
															<table style="width: 100%;" class="tablex" >
																<thead>
																	<tr>
																		<th class="tdx">
																			<div style="text-align: center;">Program</div>
																		</th>
																		<th class="tdx">
																			<div style="text-align: center;">Atas Nama</div>
																		</th>
																		<th class="tdx">
																			<div style="text-align: center;">Nomimal</div>
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="width: 33.3333%;" class="tdx">{{$row->program}}</td>
																		<td style="width: 33.3333%;" class="tdx">{{$row->on_behalf}}</td>
																		<td style="width: 33.3333%;" class="tdx">Rp. {{$row->nominal}}</td>
																	</tr>
																	<tr>
																		<td style="width: 33.3333%;">
																			<br>
																			</td>
																			<td style="width: 33.3333%;" >Jumlah</td>
																			<td style="width: 33.3333%;">
																				<strong>Rp. {{$row->nominal}}</strong>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<br>
																</td>
															</tr>
															<tr>
																<td style="width: 33.3333%;">
																	<br>
																	</td>
																	<td style="width: 18%;">
																		<br>
																		</td>
																		<td style="width: 48.4179%;">
																			<br>
																			</td>
																		</tr>
																		<tr>
																			<td style="width: 33.3333%;">
																				<br>
																				</td>
																				<td style="width: 18%;">
																					<br>
																					</td>
																					<td style="width: 48.4179%;">
																						<br>
																						</td>
																					</tr>
																					<tr>
																						<td colspan="3" style="width: 100%;">
																							<div style="text-align: center;">
																								<strong>Jazakumullah Khairan Katsiran</strong>
																							</div>
																							<p>
																								<br>
																								</p>
																								<p style="text-align: center;">Semoga Allah memberikan pahala kepadamu pada barang yang engkau berikan dan semoga Allah memberkahimu dalam harta-harta yang masih engkau sisakan dan semoga pula menjadikannya sebagai pembersih (dosa) bagimu. Aamiin</p>
																								<div data-empty="true" style="text-align: center;">
																									<br>
																									</div>
																								</td>
																							</tr>
																							<tr>
																								<td style="width: 33.3333%; vertical-align: top;">
																									<br>
																									</td>
																									<td style="width: 18%;">
																										<br>
																										</td>
																										<td style="width: 48.4179%;">
																											<br>
																											</td>
																										</tr>
																										<tr>
																											<td colspan="3" style="width: 100%;">
																												<br>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																									<p>
																										<br>
																										</p>



</body>
</html>
@endforeach