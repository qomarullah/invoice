<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->
<form method='get' id='form-market' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['field']) !!}
    <div class="input-group">
        <select onchange="$('#form-market').submit()" name='field' style="width: 300px;" class='form-control input-sm'>
        <option {{($status=="all")?'selected':''}} value='all'>- All -</option>
        @foreach($field as $row)
        {{$field=$request_field}}
         <option {{($mid==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>


<form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
    <div class="input-group">
        <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}"
                placeholder="{{trans('crudbooster.filter_search')}}"/>
        {!! CRUDBooster::getUrlParameters(['q']) !!}
        <div class="input-group-btn">
            @if(Request::get('q'))
                <?php
                $parameters = Request::all();
                unset($parameters['q']);
                $build_query = urldecode(http_build_query($parameters));
                $build_query = ($build_query) ? "?".$build_query : "";
                $build_query = (Request::all()) ? $build_query : "";
                ?>
                <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"'
                        title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
            @endif
            <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<table class='table table-striped table-dark'>
  <thead>
      <tr>
        <th>No.</th>
        <th>Client Type</th>
        <th>Email</th>
        <th>Reference</th>
        <th>Amount</th>
        <th>Type Trx</th>
        <th>Start Balance</th>
        <th>End Balance</th>
        <th>Status</th>
        <th>Status Desc</th>
        <th>Created Date</th>
        <th>Last Update</th>
        <th>Action</th>
       </tr>
  </thead>
  <tbody>
    <?php $i=1;
    ?>
    
    @foreach($result as $row)
      <?php
      $link=CRUDBooster::adminPath()."/orders?q=".$row->reference;
      if($row->reason_id=="5"){
        $link=CRUDBooster::adminPath()."/payout?q=".$row->reference;
      }
      ?>
      <tr>
        <td>{{$i++}}.</td>
        <td>{{$row->client_type}}</td>
        <td>{{$row->email}}</td>
        <td><a href="{{$link}}">{{$row->reference}}</a></td>
        <td>{{$row->amount}}</td>  
        <td>{{$row->remark}}</td>
        <td>{{$row->balance_start}}</td>
        <td>{{$row->balance_end}}</td>
        <td>{{$row->status}}</td>
        <td>{{$row->status_desc}}</td> 
        <td>{{$row->created_at}}</td>
        <td>{{$row->updated_at}}</td>
        <td>
          <!-- To make sure we have read access, wee need to validate the privilege -->
          @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif
        </td>
       </tr>
     
    @endforeach
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection