<!-- First you need to extend the CB layout -->
<meta name="csrf-token" content="{{ Session::token() }}"> 


@extends('crudbooster::admin_template')
@section('content')
<script type="text/javascript">
    //$(document).ready(function() {
      //  alert("Settings page was loaded");
    //});
    function save(id){
      var price=document.getElementById("price_"+id).value;
      //console.log("=========="+price);
      var url='{!!CRUDBooster::mainpath($slug=NULL)!!}/price/'+id+'/'+price;
      console.log(url);
      $.get(url,
        {
           '_token': $('meta[name=csrf-token]').attr('content'),
        }).error(
          function(data)
          {
            console.log('failed')
            alert("Perubahan harga gagal, coba kembali!");
          }
        ).success(
          function(data)
          {
            console.log('success')
            alert("Perubahan harga sukses!");
          }
        );
    
    }

</script>

<!-- Your custom  HTML goes here -->
<form method='get' id='form-market' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['mid']) !!}
    <div class="input-group">
        <select onchange="$('#form-market').submit()" name='mid' style="width: 300px;" class='form-control input-sm'>
        
        @foreach($market as $row)
        {{$midx=$request_mid}}
         <option {{($midx==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' id='form-category' style="display:inline-block;width: 300px;" action='{{Request::url()}}'>
    {!! CRUDBooster::getUrlParameters(['cid']) !!}
    <div class="input-group">
        <select onchange="$('#form-category').submit()" name='cid' style="width: 300px;" class='form-control input-sm'>
        <option {{($cid=="all")?'selected':''}} value='all'>- All -</option>
        @foreach($category as $row)
        {{$cid=$request_cid}}
       
         <option {{($cid==$row->id)?'selected':''}} value='{{$row->id}}'>{{$row->id}}-{{$row->name}}</option>
        @endforeach
           
        </select>
    </div>
</form>

<form method='get' style="display:inline-block;width: 260px;" action='{{Request::url()}}'>
    <div class="input-group">
        <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control input-sm pull-{{ trans('crudbooster.right') }}"
                placeholder="{{trans('crudbooster.filter_search')}}"/>
        {!! CRUDBooster::getUrlParameters(['q']) !!}
        <div class="input-group-btn">
            @if(Request::get('q'))
                <?php
                $parameters = Request::all();
                unset($parameters['q']);
                $build_query = urldecode(http_build_query($parameters));
                $build_query = ($build_query) ? "?".$build_query : "";
                $build_query = (Request::all()) ? $build_query : "";
                ?>
                <button type='button' onclick='location.href="{{ CRUDBooster::mainpath().$build_query}}"'
                        title="{{trans('crudbooster.button_reset')}}" class='btn btn-sm btn-warning'><i class='fa fa-ban'></i></button>
            @endif
            <button type='submit' class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>

        </div>
     

    </div>
    
</form>

<form method='get' style="display:inline-block;width: 260px;">
<!--<div class="input-group-btn">
    <?php
    //  $link=CRUDBooster::mainpath()."/enableAll/".$mid;
      ?>
      &nbsp;<button type='button' onclick='location.href="{{$link}}"' class='btn btn-sm btn-info'>Enable Semua Produk (harga akan reset!)</i></button>
    </div>
    -->
</form>

<table class='table table-striped table-dark'>
  <thead>
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Harga Pasar</th>
        <th>Active</th>
       <!-- <th>Enable</th>-->
        <!--<th>Pasar ID</th>-->
        <th>Unit</th>
        <th>image</th>
        <!--<th>Sort</th>-->
        <!--<th>Action</th>-->
       </tr>
  </thead>
  <tbody>
    <?php $i=1;
    ?>
  
    @foreach($result as $row)
        <?php
        $enable="No - <a href='market_products/enable/$mid/$row->id/$request_cid/$row->price/$request_search'>Enable?</a>";
        $is_active="No"; // - <a href='market_products/active/$mid/$row->id/1/$request_search'>Activate?</a>";
        $price=$row->price;
        $sort=$row->sort;
        $readonly="readonly";
        $rowid=$row->id;
        ?>

      @foreach($market_products as $rowx)
        
          
        @if($rowx->product_id==$row->id) 
        <?php
          $readonly="";
          $enable="Yes";// - <a href='market_products/enable/$mid/$row->id/$rowx->price/$request_search'>Disable?</a>";
          if($rowx->is_active=="1")
            $is_active="Yes - <a href='market_products/active/$mid/$row->id/$request_cid/0/$request_search'>Deactivate?</a>";
          else
            $is_active="No - <a href='market_products/active/$mid/$row->id/$request_cid/1/$request_search'>Activate?</a>";
          
          $price=$rowx->price_origin;
          $sort=$rowx->sort;
          $rowid=$rowx->id;
          ?>
        @endif
      
     @endforeach
      <tr>
        <td>{{$i++}}.</td>
        <td>{{$row->name}}</td>
        <!--<td>{{$mid}}</td>-->
        <td>
        <input type="number" class="form-control input-sm" value="{{$price}}" id="price_{{$rowid}}" {{$readonly}}/><input type="button" value="save" onClick="save({{$rowid}})"/></td>
        <td>{!!$is_active!!}</td>
       <!-- <td>{!!$enable!!}</td>-->
        <td>{{$row->unit}}</td>
        <td><img src='{{$row->img}}' width="100px" ></img></td>
       <!--<td>{{$sort}}</td>-->
        <td>
          <!-- To make sure we have read access, wee need to validate the privilege -->
         <!-- @if(CRUDBooster::isUpdate() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("edit/$row->id")}}'>Edit</a>
          @endif
          
          @if(CRUDBooster::isDelete() && $button_edit)
          <a class='btn btn-success btn-sm' href='{{CRUDBooster::mainpath("delete/$row->id")}}'>Delete</a>
          @endif
          -->
        </td>
       </tr>
     
    @endforeach
  </tbody>
</table>

<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection