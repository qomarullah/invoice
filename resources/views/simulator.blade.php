<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Ngepasar.com - Simulator</title>

    <!--<link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/starter-template/">-->

    <!-- Bootstrap core CSS -->
    <!--<link href="../assets/dist/css/bootstrap.css" rel="stylesheet">-->
    <link href="{{ asset("vendor/bootstrap/assets/dist/css/bootstrap.css") }}" rel="stylesheet" type="text/css"/>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/home.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
      $(document).ready(function(){
        $("#pasar").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
            
          }
         calculate();
        });
        $("#trx").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
          }
         calculate();
        });
        $("#nominal").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
          }
         calculate();
        });
        $("#cap_fee").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
          }
         calculate();
        });
        $("#squad_fee").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
          }
          
         calculate();
        });
        $("#delivery_fee").keyup(function(e) {
          console.log("Handler for .keyup() called." );
          if (/\D/g.test(this.value))
          {
            this.value = this.value.replace(/\D/g, '');
          }
         calculate();
        });
        calculate();
      });

      const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0
      })

      function calculate(){
       //alert(str);
        var pasar=document.getElementById("pasar").value;
        var trx=document.getElementById("trx").value;
        var nominal=document.getElementById("nominal").value;
        var cap_fee=document.getElementById("cap_fee").value;
        var squad_fee=document.getElementById("squad_fee").value;
        var delivery_fee=document.getElementById("delivery_fee").value;

        console.log("pasar:"+pasar);
        console.log("trx:"+trx);
        console.log("nominal:"+nominal);
        console.log("cap_fee:"+cap_fee);
        console.log("squad_fee:"+squad_fee);
        console.log("delivery_fee:"+delivery_fee);
      

        cap_fee=(100-50)/100*cap_fee;
        squad_fee=(100-50)/100*squad_fee;
        
        var pasar = parseInt(pasar);
        var final_bucket=110/100*nominal;
        console.log("final_bucket:"+final_bucket);
        var total_gmv=pasar*trx*final_bucket;
        console.log("total_gmv:"+total_gmv);
       
        var total_fee=(final_bucket-nominal)*pasar*trx;
        console.log("total_fee:"+total_fee);
       

        //var total_fee = 10/100*nominal*pasar*trx;
        var part_fee = 50/100*total_fee;
        var total_cap_feex = cap_fee/100*total_fee/pasar;
        var total_squad_feex = squad_fee/100*total_fee/pasar;



        var ongkir = delivery_fee*trx;
        console.log("ongkir:"+ongkir);


        var total_squad_fee=parseInt(total_squad_feex)+parseInt(ongkir);
        var total_cap_fee=parseInt(total_cap_feex)*pasar;

        console.log("total_fee:"+total_fee);
        console.log("part_fee:"+part_fee);
        console.log("total_cap_fee:"+total_cap_fee);
        console.log("total_squad_fee:"+total_squad_fee);
        
        document.getElementById("daily_fee_squad").value=formatter.format(total_squad_fee);
        document.getElementById("daily_fee_cap").value=formatter.format(total_cap_fee);
 
        document.getElementById("monthly_fee_squad").value=formatter.format(total_squad_fee*30);
        document.getElementById("monthly_fee_cap").value=formatter.format(total_cap_fee*30);

        document.getElementById("yearly_fee_squad").value=formatter.format(total_squad_fee*360);
        document.getElementById("yearly_fee_cap").value=formatter.format(total_cap_fee*360);

        
        
      }
     
      </script>

  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-success fixed-top">
   <a class="navbar-brand" href="https://ngepasar.com"><img src="img/logo.png"/>&nbsp;Ngepasar.com</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
        <a class="nav-link" href="register">Registrasi<span class="sr-only"></span></a>
       </li>
       <li class="nav-item active">
        <a class="nav-link" href="simulator">Simulator<span class="sr-only">(current)</span></a>
       </li> 
      <!--<li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      -->
    </ul>
    <form class="form-inline my-2 my-lg-0" method="get" action="{{CRUDBooster::adminPath()}}">
      <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
      <button class="btn btn-light my-2 my-sm-0" type="submit">login</button>
    </form>
  </div>
</nav>

<main role="main" class="container">
  <div class="row">
    <h1>Simulator</h1>
  </div>
  <div class="row">
    <p>Ayo bergabung bersama kami di <b>Ngepasar</b> menjadi bagian dari kemajuan ekonomi masyarakat di era pandemi, klik <a href="register">Registrasi</a></p>
  </div>
<hr>
  <div class="row">
    <h2>Input</h2>
  </div>
  <form action="/action_page.php">
  <div class="row">
    <div class="col-sm-3 form-group">
          <div class="row">
              <label for="pasar">Jumlah Pasar:</label>
              <input type="input" class="form-control" id="pasar" value="1">
          </div>
          <div class="row">
              <label for="trx">Jumlah Transaksi Harian:</label>
              <input type="input" class="form-control" id="trx" value="1">
          </div>
          <div class="row">
              <label for="exp">Nominal Tiap Belanja:</label>
              <input type="input" class="form-control" id="nominal" value="150000">
          </div>
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-3 form-group">
      <div class="row">
          <label for="cap_fee">% Captain Fee:</label>
          <input type="input" class="form-control" id="cap_fee" value="50">
      </div>
      <div class="row">
          <label for="squad_fee">% Squad Fee:</label>
          <input type="input" class="form-control" id="squad_fee" value="50">
      </div>
      <div class="row">
          <label for="delivery_fee">Rata Ongkir:</label>
          <input type="input" class="form-control" id="delivery_fee" value="5000">
      </div>
    </div>
  </div>

  <hr>
  
  <div class="row">
    <h2>Pendapatan Harian</h2>
  </div>
  <div class="row">
    <div class="col-sm-3 form-group">
          <div class="row">
              <label for="daily_fee_squad">Squad:</label>
              <input type="text" class="form-control" id="daily_fee_squad" readonly="true">
          </div>
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-3 form-group">
      <div class="row">
          <label for="daily_fee_captain">Captain:</label>
          <input type="text" class="form-control" id="daily_fee_cap" readonly="true">
      </div>
    </div>
  </div>

  <div class="row">
    <h2>Pendapatan Bulanan</h2>
  </div>
  <div class="row">
    <div class="col-sm-3 form-group">
          <div class="row">
              <label for="monthly_fee_squad">Squad:</label>
              <input type="text" class="form-control" id="monthly_fee_squad" readonly="true">
          </div>
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-3 form-group">
      <div class="row">
          <label for="monthly_fee_captain">Captain:</label>
          <input type="text" class="form-control" id="monthly_fee_cap" readonly="true">
      </div>
    </div>
  </div>

  <div class="row">
    <h2>Pendapatan Tahunan</h2>
  </div>
  <div class="row">
    <div class="col-sm-3 form-group">
          <div class="row">
              <label for="yearly_fee_squad">Squad:</label>
              <input type="text" class="form-control" id="yearly_fee_squad" readonly="true">
          </div>
    </div>
    <div class="col-sm-1 form-group">&nbsp;
    </div>
    <div class="col-sm-3 form-group">
      <div class="row">
          <label for="yearly_fee_captain">Captain:</label>
          <input type="text" class="form-control" id="yearly_fee_cap" readonly="true">
      </div>
    </div>
  </div>

  

  
</form>



</main><!-- /.container -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="{{ asset("vendor/bootstrap/assets/dist/js/bootstrap.bundle.js") }}"></script></body>
</html>
