<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDbooster;
use Barryvdh\DomPDF\Facade as PDF;


class AdminDonaturController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "donatur";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			if(CRUDBooster::myPrivilegeId()>2){
				$this->button_export = false;
			}

			

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"name"];
			$this->col[] = ["label"=>"Telp","name"=>"phone"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			$this->col[] = ["label"=>"Alamat","name"=>"address"];
			$this->col[] = ["label"=>"Atas Nama","name"=>"on_behalf"];
			$this->col[] = ["label"=>"Nominal","name"=>"nominal"];
			$this->col[] = ["label"=>"Payment Method","name"=>"payment_method_id" , "join"=>"payment_method,name"];
			$this->col[] = ["label"=>"Program","name"=>"program_id" , "join"=>"program,name"];
			$this->col[] = ["label"=>"Relawan","name"=>"relawan_id", "join"=>"relawan,name"];
			$this->col[] = ["label"=>"Tanggal Invoice","name"=>"invoice_date"];
			//$this->col[] = ["label"=>"Created Date","name"=>"created_at"];
			//$this->col[] = ["label"=>"Last Update","name"=>"updated_at"];


			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			//$this->form[] = ['label'=>'Nama','name'=>'donatur_name','type'=>'select2', 'datatable'=>'donatur_name,name'];
			//$this->form[] = ['label'=>'Email','name'=>'email_id','type'=>'text','datatable'=>'donatur_email,email','parent_select'=>'donatur_name_id', 'width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Search','name'=>'donatur_name','type'=>'select2', 'datatable'=>'donatur,name','datatable_format'=>"name,' - ',phone",'datatable_where'=>$this->getDataDonatur()];
			$this->form[] = ['label'=>'Nama','name'=>'name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Telp','name'=>'phone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Address','name'=>'address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Atas Nama','name'=>'on_behalf','type'=>'text','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Invoice Date','name'=>'invoice_date','type'=>'datetime','width'=>'col-sm-5','validation'=>'required|min:1|max:255'];
			$this->form[] = ['label'=>'Nominal','name'=>'nominal','type'=>'money','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Payment Method','name'=>'payment_method_id','type'=>'select2','datatable'=>'payment_method,name','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Program','name'=>'program_id','type'=>'select2','datatable'=>'program,name','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			$this->form[] = ['label'=>'Relawan','name'=>'relawan_id','type'=>'select2','datatable'=>'relawan,name','validation'=>'required|min:1|max:255','width'=>'col-sm-5'];
			
           # END FORM DO NOT REMOVE THIS LINE
		   $this->addaction[] = ['label'=>'INVOICE','url'=>CRUDBooster::mainpath('invoice/[id]'),'icon'=>'fa fa-list','color'=>'success'];


		   $this->script_js = "
			$(function() {
				//Your custom javascript/jquery goes here
				//alert('ok');
				$('#donatur_name').on('change',function(){
					//$('#email').val(\"test\");
					console.log(\"test\");

					$.ajax({
						url: ADMIN_PATH+'/donatur/'+ $(this).val(),
						type: 'get',
						
							success: function(data) {
								$('#name').val(data[0].name);
								$('#email').val(data[0].email);
								$('#phone').val(data[0].phone);
								$('#address').val(data[0].address);
								$('#on_behalf').val(data[0].name);
								//console.log(data);
								},
							error: function(jqXHR, textStatus, errorThrown) {}
							});
						
				});
					
			});
			";


	}
	public function getInvoice($id) {
			
		$data['invoice'] = DB::table('donatur')
		->join('program', 'donatur.program_id', '=', 'program.id')
		->join('relawan', 'donatur.relawan_id', '=', 'relawan.id')
		->select(DB::raw('donatur.*, program.name as program, relawan.name as relawan'))
		->where('donatur.id', '=', $id)->get();
		//$date=date_create($data['invoice'][0]->created_at);
		$date=date_create($data['invoice'][0]->invoice_date);
		$data['invoice'][0]->date=date_format($date,"d/m/Y H:i:s");
		$data['invoice'][0]->nominal=number_format($data['invoice'][0]->nominal,0,',','.');
		$name=$data['invoice'][0]->name;
		$reference=$data['invoice'][0]->reference;
		
		
		
		$html=CRUDBooster::getSetting("invoice_template");
		$html=str_replace("[reference]",$data['invoice'][0]->reference,$html);
		$html=str_replace("[name]",$data['invoice'][0]->name,$html);
		$html=str_replace("[date]",$data['invoice'][0]->date,$html);
		$html=str_replace("[nominal]",$data['invoice'][0]->nominal,$html);
		$html=str_replace("[on_behalf]",$data['invoice'][0]->on_behalf,$html);
		$html=str_replace("[address]",$data['invoice'][0]->address,$html);
		$html=str_replace("[program]",$data['invoice'][0]->program,$html);
		
		$fileName = preg_replace('/\s+/', '_', $name)."_".$reference;
		$data['html']=$html;
		//$pdf = \PDF::loadView('invoice', $data);
		$pdf = \PDF::loadHTML($html);
		$customPaper = array(0,0,360,360);
		$pdf->setPaper('a4', 'landscape');
		return $pdf->download($fileName.'.pdf');

		//PDF::loadHTML($pdf)->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf')
		//return $pdf->stream();

		//return $pdf->download($fileName.'.pdf');
		//return PDF::loadFile(public_path().'/myfile.html')->save('/path-to/my_stored_file.pdf')->stream('download.pdf');

		//This will redirect back and gives a message
		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Terjadi kesalahan download invoice, silakan hubungi admin!","info");
	 }

	 public function getData($id) {
			
		$data = DB::table('donatur')
		->select(DB::raw('*'))
		->where('id', '=', $id)->get();
		return $data;
	 
	}
	public function getDataDonatur() {
		//$data = DB::table('donatur')
		//->select(DB::raw('name,phone'))
		//->groupBy('name','phone')
		//->get();
		DB::statement("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");
		$data = DB::select('select id, name, phone from donatur group by name, phone');

		$arrayId =Array();
		for ($i=0;$i<sizeof($data);$i++){
			$arrayId[$i]=$data[$i]->id;
		}
		$listId=implode ("','" ,$arrayId);
		
		return "id in ('".$listId."')";
	 
	}
	 public function hook_before_add(&$postdata) {   

		if($postdata["on_behalf"]==""){
			$postdata["on_behalf"]=$postdata["name"];
		}
		$date=date_create($postdata["invoice_date"]);
		$inv=date_format($date,"dmYHis");
		$postdata["reference"]="INV".$inv;

	}
	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
