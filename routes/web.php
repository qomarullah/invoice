<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});


//Route::get('/','RegisterController@getIndex');
Route::get('admin/invoice/{id}/{q?}', 'AdminDonaturController@getInvoice');
Route::get('admin/donatur/{id}', 'AdminDonaturController@getData');


Route::get('admin/market_products/enable/{mid}/{id}/{cid}/{price}/{q?}', 'AdminMarketProductsController@setEnable');
Route::get('admin/market_products/enableAll/{mid}', 'AdminMarketProductsController@setEnableAll');

Route::get('admin/market_products/active/{mid}/{id}/{cid}/{is_active}/{q?}', 'AdminMarketProductsController@setActive');
Route::get('admin/market_products/price/{id}/{price}', 'AdminMarketProductsController@setPrice');

Route::get('admin/market_category/enable/{mid}/{id}/{q?}', 'AdminMarketCategoryController@setEnable');
Route::get('admin/market_category/enableAll/{mid?}', 'AdminMarketCategoryController@setEnableAll');
Route::get('admin/market_category/active/{mid}/{id}/{is_active}/{q?}', 'AdminMarketCategoryController@setActive');


Route::get('simulator','SimulatorController@getIndex');
Route::get('register', 'RegisterController@getIndex');
Route::post('registerSubmit', 'RegisterController@postSubmit');

Route::get('verify', 'VerifyController@getIndex');

